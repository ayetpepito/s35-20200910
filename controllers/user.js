const bcrypt = require('bcrypt');
const { OAuth2Client } = require('google-auth-library');

const User = require('../models/User');
const Course = require('../models/Course');
const auth = require('../auth');

const clientId = '737973757739-2id483lqpigis0k9o7ncrovkd1c9dr7l.apps.googleusercontent.com';

//check if email exist in db
module.exports.emailExists = (params) => {
	return User.find({ email: params.email }).then(result => {
		return result.length > 0 ? true: false
	})
}

//register new user
module.exports.register =  (params) => { 
	let user = new User ({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		mobileNo: params.mobileNo,
		password: bcrypt.hashSync(params.password, 10)
	})

	return user.save().then((user, err) => {
		return (err) ? false : true
	})
}

//login user
module.exports.login = (params) => {
	return User.findOne({ email: params.email }).then(user => {
		if (user === null) {
			return false;
		}

		const isPasswordMatched = bcrypt.compareSync(params.password, user.password)

		if (isPasswordMatched) {
			return { accessToken: auth.createAccessToken(user.toObject()) }
		} else {
			return false
		}
	})
}

module.exports.get = (params) => {
	return User.findById(params.userId).then(user => {
		user.password = undefined
		return user
	})
}

module.exports.enroll = (params) => {
	return User.findById(params.userId).then(user => {
		user.enrollments.push({ courseId: params.courseId })

		return user.save().then((user, err) => {
			//updating course model
			return Course.findById(params.courseId).then(course => {
				course.enrollees.push({ userId: params.userId })

				return course.save().then((course, err) =>{
					return (err) ? false : true
				})
			})
		})
	})
}

module.exports.updateDetails = (params) => {
	
}

module.exports.changePassword = (params) => {
	
}

module.exports.verifyGoogleTokenId = async (params) => {
	const client = new OAuth2Client(clientId)
	const data = await client.verifyIdToken({
		idToken: tokenId,
		audience: clientId
	})


if(data.payload.email_verified === true){
	const user = await User.findOne({ email: data.payload.email }).exec();

	if (user !== null) {
		if (user.loginType === "google") {
			return {
				accessToken: auth.createAccessToken(user.toObject())
			}
		} else {
			return { error: "login-type-error" }
		}
	} else {
		const newUser = new User({
			firstName: data.payload.given_name,
			lastName: data.payload.family_name,
			email: data.payload.email,
			loginType: "google"
		}
		)

		return newUser.save().then((user, err) => {
			return {
				accessToken: auth.createAccessToken(user.toObject())
			}
		}
		)
	}
} else {
	return {error: "google-auth-error" }
}
}

// module.exports.register =  (params) => { 
// 	let user = new User ({
// 		firstName: params.firstName,
// 		lastName: params.lastName,
// 		email: params.email,
// 		mobileNumber: params.mobileNumber,
// 		password: bcrypt.hashSync(params.password, 10)
// 	})

// 	return user.save().then((user, err) => {
// 		return (err) ? false : true
// 	})
// }



//params.password = ayet123
//user.password = $2b$10$ojQWbAQTHqNPcaVRXkooiePuidR5XL3bTJJ05zEDgDSAE/lsrQf0q



