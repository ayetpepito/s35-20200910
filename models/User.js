const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First name is required']
	}, 
	lastName: {
		type: String,
		required: [true, 'Last name is required']
	},
	email: {
		type: String,
		required: [true, 'Email is required']
	},
	password: {
		type: String,
		required: [true, 'Password is required']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, 'Mobile Number is required']
	},
	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, 'Course ID is required']
			},
			enrolledOn: {
				type: Date,
				default: new Date()//new Date means YYYY-DD-MMM
			},
			status: {
				type: String,
				default: "Enrolled"
			}
		}
	]
})
//to export and can be use in other other file
module.exports = mongoose.model('User', userSchema);
//create route(endpoints or url we access)