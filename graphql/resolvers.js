const UserController = require('../controllers/user');
const CourseController = require('../controllers/course');

module.exports = {
	Query: {
		emailExists: (parent, args) => { // args = arguments - dito nasasave yung nasa schema
		return UserController.emailExists(args) // (args) - dito napapasa yung args sa taas
		},
		getAllCourses: (parent, args) => {
			return CourseController.getAll()
		},
		getCourse: (parent, args) => {
			return CourseController.get(args)
		},
		getUser: (parent, args, context) => {
			guardResolver(context.currentUser)
			return UserController.get({ userId: context.currentUser.id})
		}
	},
	Mutation: {
		register: (parent, args) => {
			return UserController.register(args)
		}, 
		login: (parent, args) => {
			return UserController.login(args)
		},
		addCourse: (parent, args, context) => {
			guardResolver(context.currentUser)
			return CourseController.add(args)
		},
		enroll: (parent, args, context) => {
			guardResolver(context.currentUser)
			return UserController.enroll({
				userId: context.currentUser.id,
				courseId: args.courseId
			})
		},
		updateUserDetails: (parent, args, context) => {
			guardResolver(context.currentUser)
		},
		updateUserPassword: (parent, args, context) => {
			guardResolver(context.currentUser)
		},
		verifyGoogleTokenId: (parent, args, context) => {
			guardResolver(context.currentUser)
		},
		archive: (parent, args) => {
			return CourseController.archive(args)
		}
		,
		updateCourse: (parent, args) => {
			return CourseController.update(args)
		}
	}
}

const guardResolver = (currentUser) => {
	if (currentUser === null) {
		throw new Error('Authentication failure')
	}
}

// context from index.js, from req na pinapasa, sinasabi na ireturn ng ey valeu na user at auth.decode - obj na dinecode from token

// context sa resolver - napupunta is any info that we need,  run as a function na gusto natin ireturn 

// throw in js - conjjunction with error,  creating new error with message authentication

// const UserController = require('../controllers/user');

// module.exports = {
// 	Query: {
// 		emailExists: (parent, args) => {
// 			return UserController.emailExists(args)
// 		}
// 	},
// 	Mutation: {
// 		register: (parent, args) => {
// 			return UserController.register(args)
// 		}
// 	}
// }

// argument yung pinapasa nating data from sa schema will be saved in argumant
// nirereturn to controller then call method pinapasa ung information
// any changes in information, post, patch delete will be in mutation, also get request